<?php

/**
 * @file
 * Proxy Processor Plugin: proxy_processor_remove_scripts
 */

/**
 * Implementation of Proxy Processor Plugin
 */
class proxy_processor_remove_scripts {
  var $options;

  /**
   * Initiate any options
   *
   * Define any default options that can be stored
   * in the options property of the class.
   *
   * @return
   *   Array of options
   */
  function options_init() {
    return array();
  }

  /**
   * Options form
   *
   * Defines any form items needed for
   * options of the processor.
   *
   * @return
   *   Valid Drupal form array.
   */
  function options_form() {
    return array();
  }

  /**
   * Main Rendering Function
   *
   * @param $response
   *   Response object to process.  Main content
   *   is in $response->data
   * @param $params
   *   Array of parameters, as sent to proxy().
   */
  function render(&$response, $params) {
    $content = $response->data;
    // Set up arrays for matching
    // Find anything looking like a script tag and discard it.
    $matches[] = '#<script[\s\S]*?</script>#';
    $replaces[] = '';
    // Change content
    $response->data = preg_replace($matches, $replaces, $response->data);
  }
}
