(function ($) {
///////////////////////////////////////////////////////////////////////////////

/**
 * Script library to perform actions (based losely on Selenium scripts)
 * on a nearby iframe.
 * 
 * Something of a SeleniumRunner remade in jquery
 */


/**
 * An object that handles communication with the target window.
 * 
 * Modelled initially on the Selenium BrowserBot js
 * 
 * @param topLevelApplicationWindow
 * @returns
 */
BrowserBot = function(topLevelApplicationWindow, options) {
  this.defaults = {
    blockerText : '<h2 style="opacity:1; text-align:center; padding-top:2em; padding-bottom:2em; background-color:#FFAAAA; opacity:0.8;">Hang about, loading and analysing page...</h2>',
    blockerStyle : "background-color:red; opacity:0.5; ",
    scalePreview : '66%',
    proxyBase : '',
    sourceUriBase : '/',
    sourceURIBaseFormElementID : '',
  };
  this.options = $.extend(this.defaults, options);


  // I'd rather use <object> but cannot yet catch its onload event
  //if (! $(topLevelApplicationWindow).is('iframe')) {
  //  $.log("Passed handle is not a window", 'error');
  //}
  this.topWindow = topLevelApplicationWindow;
  this.topFrame = this.topWindow;
  this.loc = parseUrl(window.location.href);
  // Clean this up
  this.loc['hash'] = '';
  this.baseUrl = reassembleLocation(this.loc);
  // Where to load css from:
  this.resourcePath = this.baseUrl;

  this.topWindowID = topLevelApplicationWindow.attr('id');
  // the buttonWindow is the Controller window
  // it contains the Run/Pause buttons... this should *not* be the AUT window
  this.buttonWindow = window;
  this.currentWindow = this.topWindow;
  this.currentWindowName = null;
  this.allowNativeXpath = true;

  this.recordedAlerts = new Array();
  this.recordedConfirmations = new Array();
  this.recordedPrompts = new Array();
  this.openedWindows = {};
  this.newPageLoaded = false;
  this.pageLoadError = null;

  this.shouldHighlightLocatedElement = true;
  var self = this;

  // use load() not ready() for iframe
  this.topWindow.load(function () {
    $.log('Window load');
    self.initializeWindow();
  });
  this.topWindow.unload(function(e){
    $.log('Frame unloading');
    director.uninitializeWindow();
  });

  $.log('browserBot ready. ');
  $.log(this.topWindow);
};

/**
 * Run this when the target window is loaded or reloaded. 
 * Ensure our handle is up to date.
 * 
 * Usually triggered from the frames onload.
 */
BrowserBot.prototype.initializeWindow = function() {
  $.log('Initializing window', 'group');
  if (this.options.scalePreview) {
    $('html', this.getCurrentWindow().contents()).css('zoom', this.options.scalePreview);
  }
  //this.rewriteAbsoluteLinks(original_proxied_url, '/');
  var selections_css = this.options.resourcePath + '/browserbot-selections.css';
  this.injectCSS(selections_css);
  
  // Page may have been dulled while processing/waiting for it. focus it again.
  var win = this.getCurrentWindow();
  win.css('opacity', 1);
  $('#windowBlocker').remove();

  // register an unload function. This doesn't seem to work from above,
  // Needs to be done fresh each load?
  /*
  $(frames['wrapped']).bind('beforeunload', function(e) { 
    $.log('Frame beforeunload');
    director.uninitializeWindow()
  });
  */
  
  // Can't seem to use jquery to get the right handle.
  // Use the global frames array instead.
  $(frames[this.topWindowID]).unload(function(e){
    $.log('Frame unload');
    director.uninitializeWindow();
  });
  
  // Update any display of this URI
  this.options.sourceUriBase = this.getOpenLocation();
  if (this.options.sourceURIBaseFormElementID) {
    $(this.options.sourceURIBaseFormElementID).val(this.options.sourceUriBase);
  }
  $.log('Window initialized. ID ' + this.options.sourceURIBaseFormElementID);
  
  $.log('Window initialized. Current location is ' + this.getOpenLocation(), 'grend');
};

/**
 * Window is going away for a while.
 * Usually triggered from the frames onload, like when a user clicks a button inside the site.
 * 
 * Need to detach our controllers
 */
BrowserBot.prototype.uninitializeWindow = function() {
  $.log('unInitializing window');

  // Visually block access to the loading page
  $.log('Obscuring the window while load happens');
  var win = this.getCurrentWindow();
  win.css('opacity', 0.5);
  
  this.windowBlocker = $('<div id="windowBlocker" style="position:absolute;"><div id="windowBlockerInner" style="position:absolute; ' + this.options.blockerStyle + '" ></div>' + this.options.blockerText + '</div>');
  $('body').append(this.windowBlocker);
  this.windowBlocker
    .width(win.width())
    .height(win.height())
    .css(win.offset());
  $('#windowBlockerInner')
    .width(win.width())
    .height(win.height());
    //.css(win.offset());
  
};

/**
 * Add a css file to the head.
 */
BrowserBot.prototype.injectCSS = function(selections_css) {
  $('head', this.getCurrentWindow().contents()).append('<link rel="stylesheet" href="' + selections_css + '" type="text/css" />');
  $.log('Added custom css ' + selections_css + ' to the document');
}

/**
 * Check if the target window has full URLs. 
 * 
 * As it's proxied, these need to be rewritten to be at least root-relative
 */
BrowserBot.prototype.rewriteAbsoluteLinks = function(original_proxied_url, target_url) {
  $.log('rewriting absolute links from ' + original_proxied_url + " to " + target_url);
  // If there is a 'base', throw that away
  $(this.currentWindow).contents().find("base").remove();
  
  // seem to need contents().find() ?
  // normal way with context also works
  // $('a', $(this.currentWindow).contents()).each(
  
  $(this.currentWindow).contents().find("a").each(
    function() {
      href = "" + $(this).attr('href');
      if (href.match(original_proxied_url)) {
        var replacement = href.replace(original_proxied_url, target_url);
        //$.log('replacing link ' + href + ' with a non-absolute version: ' + replacement, 'debug');
        $(this).attr('href', replacement);
        // just for diagnostics :
        $(this).css('font-style', 'italic');
      }
    }
  );
};


/**
 * Given an xpath, search on the document and return the element list
 * (seems that xpaths was a reserved word?
 */
BrowserBot.prototype.findElement = function(xpathPattern) {
  $.log('Searching document for matches to : ' + xpathPattern);
  return $(this.currentWindow).contents().find(xpathPattern);
};


/**
 * Add a mouseover highlight & select action to all divs on the page
 */
BrowserBot.prototype.sensitizeElements = function() {
  $.log('Sensitizing container elements');
  var elements = $(this.currentWindow).contents().find("div");
  elements
    .mouseover(this.mouseoverElement)
    .mouseout(this.mouseoutElement)
    .rightClick(this.chooseElement);
};

/**
 * Add a mouseover highlight
 */
BrowserBot.prototype.mouseoverElement = function(event) {
  //$.log('highlighting element');
  director.highlightElement($(event.target));
  // set this as the currently chosen element
};
BrowserBot.prototype.highlightElement = function(element) {
  element.addClass('highlighted');
};

/**
 * Remove the mouseover highlight
 */
BrowserBot.prototype.mouseoutElement = function(event) {
  //$.log('unhighlighting element');
  director.unhighlightElement($(event.target));
  // unset this as the currently chosen element
};
BrowserBot.prototype.unhighlightElement = function(element) {
  element.removeClass('highlighted');
};


/**
 * Toggle the currently selected state
 */
BrowserBot.prototype.chooseElement = function(event) {
  element = $(event.target);
  // Rightclick library passes me the DOM element, not the jquery one?
  $.log('selecting element');
  $.log(element);
  if (element.hasClass('chosen')) {
    element.removeClass('chosen');
  }
  else {
    element.addClass('chosen');
  }
};

BrowserBot.prototype.getCurrentWindow = function() {
  return this.currentWindow;
};

BrowserBot.prototype.getOpenLocation = function() {
  // read the actual current URL, and subtract the proxy prefix.
  var URI = this.getCurrentWindow().get(0).contentWindow.location.href
  if (this.options.proxyBase) {
    return URI.replace(this.options.proxyBase, '')
  }
  return URI;
  return this.getCurrentWindow().attr('src');
};

/**
 * Opens a given URL in the window
 */
BrowserBot.prototype.openLocation = function(target) {
  // We're moving to a new page - clear the current one
  var win = this.getCurrentWindow();
  win.css('opacity' ,0.3);
  this.newPageLoaded = false;
  this.setOpenLocation(win, target);
};


BrowserBot.prototype.setOpenLocation = function(win, loc) {
  loc = absolutify(loc, this.baseUrl);
  $.log('Opening ' + loc + ' in window');
  if (this.options.proxyBase) {
    $.log('Opening ' + loc + ' using proxy');
    win.attr('src', this.options.proxyBase + loc);
  }
  else {
    win.attr('src', loc);
  }
};


/**
 * Returns a jquery list of all links in the current window
 * Interim test utility
 */
BrowserBot.prototype.enumerateLinks = function() {
  $.log('enumerating links');
  var links = $(this.currentWindow).contents().find("a");
  alert(links.length + " links found in the source page");
  return links;
};


///////////////////////////////////////////////////////////////////////////////
})(jQuery);
