(function ($) {
///////////////////////////////////////////////////////////////////////////////
console.log('Setting up logger');

// Let us use $.log() for quick messages
$.log = function (msg) {
  console.log(msg);
  return this;
};


/**
 * LOG object is for compatability with selenium code being ported
 * http://plugins.jquery.com/project/jLog seems an OK replacement
 * 
 * In the meantime, this is a stub.
 */
var Logger = function() {
}

Logger.prototype = {
  debug: function(message) {
    $.log(message, "debug");
  },
  error: function(message) {
    $.log(message, "error");
  },
  warning: function(message) {
    $.log(message, "warning");
  }
}
var LOG = new Logger();



///////////////////////////////////////////////////////////////////////////////
})(jQuery);
