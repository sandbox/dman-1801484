/*
 * Minimal XPath Syntax Compatibility Plugin for jQuery 
 * 
 * They removed real XPath from jquery :-(
 * Bring a little of it back!
 * 
 * Pretty limited and naive, just get back some basic xpath syntax
 * and convert it to the css style selectors, then carry on as before.
 * This overrides the old find() function, but renames it and uses it natively.
 * 
 * No xpath functions etc.
 * 
 * With reference to 2007 code by John Resig
 * http://plugins.jquery.com/project/xpath
 * 
 * This remake does even less than that one, but addresses:
 * replacing // with space
 * replacing / with >
 * replacing '@attributes' with 'attributes'
 * 
 * I'm building tools that should be using the same xpath expressions 
 * front end and back end (data scraper) so really would prefer to keep
 * using real syntax, not the jquery hybrid.
 * 
 * @author Dan Morrison http://coders.co.nz/
 * @version 2011-11
 * 
 * Dual licensed under MIT and GPL.
 */
(function( $ ) {
  
  $.fn.xpathQuery = function(selector, context) {
    
    // Convert // to " "
    selector = selector.replace(/\/\//g, " ");

    // Convert / to >
    selector = selector.replace(/\//g, ">");

    // Extremely naively convert [@attr] or [@attr='val'] into [attr]
    selector = selector.replace(/@/g, '');
    
    return this.oldFind(selector, context);
    //return this.find(selector, context);
  };
  
  // Now take over the old method entirely !!
  $.fn.oldFind = $.fn.find;
  $.fn.find = $.fn.xpathQuery;
})( jQuery );

