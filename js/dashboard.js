(function ($) {
///////////////////////////////////////////////////////////////////////////////

/**
 * Functions to set up a dashboard with action buttons.
 * 
 * Initialize a dashboard by going 
 * dashboard = new dashboardControl($('#selector'));
 * 
 * Requires jquery.ui.button
 */

/**
 * Static function to add buttons to the dashboard, whether or not it has been
 * initialized yet.
 * 
 * @param buttonID
 * @param buttonAction
 */
function dashboard_addButton(buttonID, buttonAction) {
  if (dashboard) {
    dashboard.addButton(buttonID, buttonAction);
  } else {
    // Put it in the list to be used when dashboard is ready to initialize
    dashboardButtons[buttonID] = buttonAction;
  }
}
  
/**
 * An object that handles button actions
 * 
 * @param container the container the dashboard will be built inside.
 */
DashboardControl = function(container, options) {
  this.name = 'ui-dashboard';
  this.defaults = {
    elementID : 'ui-dashboard',
    uiClasses : "ui-dashboard ui-widget ui-widget-content ui-corner-all "
  };
  this.options = this.defaults;
  
  if (container) {
    this.container = container;
  }
  else {
    this.container = $('body');
  }
  this.build();
  this.init(this.element, options);
};

/**
 * Set options for this object.
 */
DashboardControl.prototype.init = function(opts) {
  this.options = $.extend(this.defaults, opts);
  // Add a handle to this, the controller, to the element
  // http://jupiterjs.com/news/writing-the-perfect-jquery-plugin
  this.element.data(this.name, this);
  this.element.addClass(this.name);
  $.log('Dashboard '+this.name+' initialized');
};

/**
 * Styles and positions the dashboard on the page.
 * Adds it to its parent container.
 */
DashboardControl.prototype.build = function() {
  this.element = $('<div>').attr('id' , this.options.elementID).addClass(this.options.uiClasses)
    .append($('<div>').attr('id', this.options.elementID + '-status').addClass('ui-dashboard-status ui-widget'));
  this.container.append(this.element);
  $.log('Dashboard built');
};

/**
 * Adds a list of buttons to the dashboard
 */
DashboardControl.prototype.addButtons = function(buttonList) {
  for (buttonID in buttonList) {
    this.addButton(buttonID, buttonList[buttonID]);
  }
  $.log('Dashboard buttons added');
};

/**
 * Add a single button to the toolbar.
 */
DashboardControl.prototype.addButton = function(buttonID, buttonAction) {
  // If I use <botton> I can't find how to stop the form submitting
  // event.preventDefault() is not working on buttons (in Chrome?)
  var button = $('<span>')
   .text(buttonID)
   .click(buttonAction)
   .button();
  this.element.append(button);
};


///////////////////////////////////////////////////////////////////////////////
})(jQuery);