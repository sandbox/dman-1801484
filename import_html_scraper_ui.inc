<?php
/**
 * UI and form for the wizard
 *
 * MOST smarts are in js, not in Drupal FAPI
 * The FAPI just provides the initial build and the serialization.
 */
function import_html_scraper_form($form, &$form_state) {
  $source_uri = variable_get('import_html_scraper_source_uri', 'http://drupal.org/');
  $profile_id = import_html_current_profile_id();
  $profile = import_html_profile_load($profile_id);

  $proxy_base = base_path() . PROXY_ROUTER_PATH . '?' . PROXY_ROUTER_QUERY_VAR . '=';
  $proxy_base = url(PROXY_ROUTER_PATH, array('absolute' => TRUE, 'query' => array(PROXY_ROUTER_QUERY_VAR => ''))) ;
  // The client front-end will need to know and use this also

  $proxied_source_uri = $proxy_base . $source_uri;
  $preview_uri = url('node/add/page', array('query' => array('no_toolbar' => TRUE)));

  // Build the form, including iframes with the previews;
  $form = array(
  );
  $form['#attached']['css'][] = drupal_get_path('module', 'import_html_scraper') . '/import_html_scraper.css';

  $form['overview'] = array(
    '#type' => 'html_tag',
    '#tag' => 'h3',
    '#value' => 'Compare the source and the result',
  );

  $form['uri-container'] = array(
    '#type' => 'html_element',
    '#tag' => 'div',
  );

  $form['uri-container']['source_uri_base'] = array(
    '#title' => t('Source URI '),
    '#type' => 'textfield',
    '#default_value' => $source_uri,
  );
  $form['uri-container']['save'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#attributes' => array('class' => array('ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only')),
  );

  $form['source-container'] = array(
    '#type' => 'html_element',
    '#tag' => 'div',
    'source-frame' => array(
      '#type' => 'html_element',
      '#tag' => 'iframe',
      '#attributes' => array(
        'src' => $proxied_source_uri,
      ),
    ),
  );

  $form['mapping-container'] = array(
    '#type' => 'html_element',
    '#tag' => 'div',
    '#attributes' => array(
    ),
    #'#value' => 'patterns go here',
  );

  $form['preview-container'] = array(
    '#type' => 'html_element',
    '#tag' => 'div',
    'preview-frame' => array(
      '#type' => 'html_element',
      '#tag' => 'iframe',
      '#attributes' => array(
        'src' => $preview_uri,
      ),
    ),
  );


  $form['toolbar'] = array(
    '#type' => 'html_element',
    '#tag' => 'div',
    #'#value' => 'Dashboard Toolbar Controls',
  );

  // Add js tools and libraries
  $module_path = drupal_get_path('module', 'import_html_scraper');

  // This is a debug tool
  $form['#attached']['library'][] = array('import_html_scraper', 'logging');
  // This repairs xpath handling that was broken in a jquery update
  $form['#attached']['library'][] = array('import_html_scraper', 'jquery.xpath');
  // Utility that create an xpath based on what element is selected,
  // used for 'show elements with IDs' button
  // Still in testing, there may be others better.
  // BETA!
  $form['#attached']['js'][] =  $module_path . '/scraper/xpathutils.js';
  // BETA!
  // Adds right-clich inspection to elements highlighted by 'show elements with id'
  // Depends on jquery-1.6.2.min.js
  $form['#attached']['js'][] =  $module_path . '/js/jquery.contextmenu-ui.js';

  // Enhancements for the UI
  $form['#attached']['library'][] = array('system', 'ui.accordion');
  // Required for the dashboard
  $form['#attached']['library'][] = array('system', 'ui.button');
  $form['#attached']['library'][] = array('system', 'ui.dialog');

  // CUSTOM scripting actually starts here.

  // This manages buttons added to the page on-the-fly.
  // Most active controls operate post-load and without using the FAPI.
  $form['#attached']['library'][] = array('import_html_scraper', 'dashboard');

  // Utility object - routines for controlling iframe content and registering load and unload events.
  $form['#attached']['library'][] = array('import_html_scraper', 'browserbot');

  // Utility object definition - app-specific functions
  $form['#attached']['js'][] =  $module_path . '/scraper.js';

  // Interim setup data
  $form['#attached']['js'][] =  $module_path . '/import_html_scraper_settings.js';

  // Actual startup initializes the UI and the scraper tool
  $form['#attached']['js'][] =  $module_path . '/import_html_scraper.js';

  // The tools sometimes call in css of their own,
  // so they need to know where to get it.
  #$form['#attached']
  drupal_add_js(array('browserbot_resource_path' => $module_path . '/js'), 'setting');
  // Pass our drupal variables to the client that may need them.
  drupal_add_js(array('proxy_base' => $proxy_base), 'setting');



  return $form;
}


function import_html_scraper_form_submit($form, &$form_state) {
  if (isset($form_state['values']['source_uri_base'])) {
    variable_set('import_html_scraper_source_uri', $form_state['values']['source_uri_base']);
    drupal_set_message('Saved the source_uri_base ' . $form_state['values']['source_uri_base']);
  }
}

/**
 * Return a dump of the current data mappings to use for a given import profile
 */
function import_html_scraper_mappings($profile, $format = 'json') {
  $mappings = array();
  $entity_type = 'node';
  $bundle = 'page';
  $format = 'php';

  if (module_exists('rdf') && $profile['rdf_mapping']) {
    $mappings += rdf_mapping_load($entity_type, $bundle);
  }
  // We have our own explicit manual mapping also.
  // Uses the same format.
  if ($profile['manual_mapping']) {
    $mappings += $profile['manual_field_mappings'];
  }
  switch ($format) {
    case 'json' :
      drupal_json_output($mappings);
      exit;
    case 'php' :
      return '<pre>' . print_r($mappings, 1) . '</pre>';
      exit;
  }


}

