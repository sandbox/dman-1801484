<?php

/**
 * Minor theme - a slightly better version of
 * theme_html_tag()
 *
 * Identical, only it ALSO supports #children
 */
function theme_html_element($variables) {
  $element = $variables['element'];
  if (empty($element['#attributes']['id'])) {
    $element['#attributes']['id'] = $element['#id'];
  }
  // I have to build my own #children string it seems.
  // I thought it was automatic, but I guess not.
  // @see drupal_render() - if an element has a #theme function,
  // Then any children of the element have to be rendered there.
  $element['#children'] .= drupal_render_children($element);

  $attributes = isset($element['#attributes']) ? drupal_attributes($element['#attributes']) : '';
  if (!isset($element['#value']) && !isset($element['#children'])) {
    return '<' . $element['#tag'] . $attributes . " />\n";
  }
  else {
    $output = '<' . $element['#tag'] . $attributes . '>';
    if (isset($element['#value_prefix'])) {
      $output .= $element['#value_prefix'];
    }
    if (isset($element['#children'])) {
      $output .= $element['#children'];
    }
    // #value is preferred. but #markup is equivalent.
    // For consistency with theme_fieldset, #value comes AFTER #children.
    if (isset($element['#markup'])) {
      $output .= $element['#markup'];
    }
    if (isset($element['#value'])) {
      $output .= $element['#value'];
    }
    if (isset($element['#value_suffix'])) {
      $output .= $element['#value_suffix'];
    }
    $output .= '</' . $element['#tag'] . ">\n";
    return $output;
  }
}

/*
function html_element_process_html_element(&$element, &$form_state) {}
function html_element_pre_render_html_element(&$element) {}
*/