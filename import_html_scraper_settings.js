/**
 * Analysis tools for scraping a page content and theme
 * 
 * Provides a form detailing scraped sections and data
 */

// List of buttons that will be added to the dashboard

/**
 * These will be pulled from JSON another time. For now, start static
 * 
 * WATCH OUT for the trailing commas. Semi-legal javascript, illegal JSON. And
 * it causes the IDE syntax parser tool to fail
 */
var scraperRules = new Array();

// Build this to be analogous with a feeds parser config if possible
// See a features export of a $feeds_importer->config for the target structure
// some extensions (strings into arrays) are made
scraperRules['Default'] = {
  // FEEDS : useful label
  'name' : 'Default',
  // FEEDS : useful description
  'description' : 'Import any page',
  // SCRAPER : The URL pattern that matches this set of extraction patterns
  'pathPattern' : '.*',

  // Feeds has the parser (extraction pattern) and the processor (mapping
  // targets)
  // in different arrays. *sigh* OK, try that. It's an extra layer of key
  // duplication.
  // A key in the parser->config->sources should be matched with a key in the
  // processor->config->mappings.
  // Where it comes from and where it goes to are handled in separate threads
  // later.
  // This is OK, but makes our configs overly layered. NVM, follow that
  // structure anyway.
  // I'll cheat by re-using internal array keys that match.
  // parser.config.sources.{mykey} matches processor.config.mappings.{mykey}
  // although really the mappings should lookup their 'source' key instead.

  // FEEDS : the data structure used by feeds.
  'parser' : {
    'config' : {
      'sources' : {
        // arbitrary key of my own making. Feeds uses any ID key
        'The page title' : {
          // I'm using an array here, feeds expects a single string
          'Page H1' : 'h1',
          'Page TITLE from head' : 'title'
        },
        'The page body' : {
          //'an element called BodyContent' : '#BodyContent',
          //'2an element called BodyContent' : '*[id=BodyContent]',
          'an element called BodyContent' : '//*[id="BodyContent"]',
          'A wordpress post body' : '.post .entry',
          'A Drupal node-body text' : '#main-content .node-body'
        },
        'Sidebar / pullquote' : {
          // I'm using an array here, feeds expects a single string
          'Last column in main content' : '.post .entry .su-column-last',
        },
      }
    }
  },
  // FEEDS : the data structure used by feeds.
  'processor' : {
    'config' : {
      'mappings' : {
        'The page title' : {
          'source' : 'The page title',
          // Drupal property
          'target' : 'title',
          // RDF property
          'mapping' : 'dc:title'
        },
        'The page body' : {
          'source' : 'The page body',
          // Drupal property
          'target' : 'body',
          // RDF property
          'mapping' : 'content:encoded'
        }
      }
    // mappings
    }
  // config
  }, // processor

  // The elements that can be extracted from this page
  'elements' : {
    // Define the extraction rule for this element
    'Page title' : {
      // RDF mapping, used a a unique key
      'mapping' : 'dc:title',
      // The xpath to search for. May be multiple
      'xpaths' : {
        // Describe the pattern
        'h1' : 'Page H1'
      }
    },
    'Page body' : {
      'mapping' : 'content:encoded',
      'xpaths' : {
        '#BodyContent' : 'an element called BodyContent'
      }
    }
  }
};

scraperRules['Topic pages'] = {
  // The URL pattern that matches this set of extraction patterns
  'pathPattern' : '.*',
  'elements' : {
    'Page title' : {
      'mapping' : 'dc:title',
      'xpaths' : {
        'h1' : 'Page H1'
      }
    },
    'Page body' : {
      'mapping' : 'content:encoded',
      'xpaths' : {
        '#BodyContent' : 'an element called BodyContent'
      }
    }
  }
};
var scraperRuleTemplates = new Array();

scraperRuleTemplates['Page title'] = {
  'mapping' : 'dc:title',
  'xpaths' : {
    'h1' : 'Page H1',
    'div[@class="title"]' : 'a div called title',
    'title' : 'title from the HTML head'
  }
};
scraperRuleTemplates['Page body'] = {
  'mapping' : 'content:encoded',
  'xpaths' : {
    '#content' : 'an element called content',
    '#BodyContent' : 'an element called BodyContent',
    'div[@class="main"]' : 'a div with the class "main"'
  }
};

