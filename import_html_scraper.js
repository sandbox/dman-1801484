(function ($) {
///////////////////////////////////////////////////////////////////////////////

/**
 * Analysis tools for scraping a page content and theme
 * 
 * Provides a form detailing scraped sections and data
 */

/**
 * Setup actions to trigger when the page is ready
 */
Drupal.behaviors.scraper = {
  // Onload actions:    
  attach: function (context, settings) {
    console.log('scraper attach');
    console.log($);

    $.log('Starting to init the dashboard', 'group');
  
    dashboard = new DashboardControl($('#edit-toolbar'));
    dashboardButtons = {};
    dashboard.addButtons(dashboardButtons);
    $.log('Dashboard ready', 'grend');

    $.log('Starting to init the scraper form', 'group');
    scraper = new ScraperControl($('#edit-mapping-container'));
    scraper.loadRules(scraperRules);
    scraper.build();
    $.log('Scraper ready', 'grend');
    
    // When the base URI is changed, tell the UI
    $('#edit-source-uri-base').change(function() {
      director.options.sourceUriBase = $('#edit-source-uri-base').val();
    });
    
    var targetFrameID = 'edit-source-frame';
    $.log('Starting to init the director browserbot. Attaching the director to  '+targetFrameID, 'group');
    wrapped_window = $('#' + targetFrameID).first();
    browserBotOptions = {
      proxyBase : Drupal.settings.proxy_base,
      sourceUriBase : $('#edit-source-uri-base').val(),
      sourceURIBaseFormElementID : '#edit-source-uri-base',
    };
    director = new BrowserBot(wrapped_window, browserBotOptions);
    // Tell it where to load css from
    director.options.resourcePath = Drupal.settings.browserbot_resource_path;
    director.options.scalePreview = "50%";
    
    // Publish some buttons to the dashboard
    //dashboard.addButton('Load siteroot', function(){director.openLocation('/');});
    dashboard.addButton('Load source URI', function(){director.openLocation(director.options.sourceUriBase);});
    dashboard.addButton('Show elements with IDs', function(e){scraper.showIDs();});
    dashboard.addButton('Highlight Elements', function(){director.sensitizeElements();});
    dashboard.addButton('Enumerate Links', function(){director.enumerateLinks();});
  
    var currentLocation = director.getOpenLocation();
    $.log('Director ready. Current frame location is ' + currentLocation, 'grend');
    
  }
}


///////////////////////////////////////////////////////////////////////////////
})(jQuery);

