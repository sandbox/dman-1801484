/**
 * set up a dashboard with action buttons
 */

// List of buttons that will be added to the dashboard
var dashboardButtons = new Array();
// handle on the dashboard controller object
var dashboard;

/**
 * Static function to add buttons to the dashboard, whether or not it has been
 * initialized yet.
 * 
 * @param buttonID
 * @param buttonAction
 */
// Buttons trigger actions for the director object to perform
function dashboard_addButton(buttonID, buttonAction) {
  if (dashboard) {
    dashboard.addButton(buttonID, buttonAction);
  } else {
    // Put it in the list to be used when dashboard is ready to initialize
    dashboardButtons[buttonID] = buttonAction;
  }
}

$(document).ready(function() {
  $.log('Starting to init the dashboard', 'group');
  // Add my own css
  var my_css = 'dashboard.css';
  $('head').append('<link rel="stylesheet" href="' + my_css + '" type="text/css" />');

  dashboard = new DashboardControl();
  dashboard.build();
  dashboard.addButtons(dashboardButtons);
  $.log('Dashboard ready', 'grend');
});


  
/**
 * An object that handles button actions
 * 
 */
var DashboardControl = function(element, options) {
  this.name = 'ui-dashboard';
  this.defaults = {
    elementID : 'ui-dashboard',
    uiClasses : "ui-dashboard ui-widget ui-widget-content ui-corner-all "
  };
  this.options = this.defaults;

  
  if (element) {
    this.init(element, options);
    this.element = element;
  }
};

DashboardControl.prototype.init = function(opts) {
  this.options = $.extend(this.defaults, opts);
  // Add a handle to this, the controller, to the element
  // http://jupiterjs.com/news/writing-the-perfect-jquery-plugin
  this.element.data(this.name,this);
  this.element.addClass(this.name);
  $.log('Dashboard '+this.name+' initialized');
};

/**
 * Styles and positions the dashboard on the page
 */
DashboardControl.prototype.build = function() {
  this.element = $('<div>').attr('id' , this.options.elementID).addClass(this.options.uiClasses)
    .append($('<div>').attr('id', this.options.elementID + '-status').addClass('ui-dashboard-status ui-widget'));
  $('body').append(this.element);
  this.init();
  $.log('Dashboard built');
};

/**
 * Adds a list of buttons to the dashboard
 */
DashboardControl.prototype.addButtons = function(buttonList) {
  for (buttonID in buttonList) {
    this.addButton(buttonID, buttonList[buttonID]);
  }
  $.log('Dasboard buttons added');
};
DashboardControl.prototype.addButton = function(buttonID, buttonAction) {
  var button = $('<button>')
   .text(buttonID)
   .click(buttonAction)
   .button();
  this.element.append(button);
};

