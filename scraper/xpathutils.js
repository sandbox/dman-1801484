/**
 * Stolen from firebug
 * http://code.google.com/p/fbug/source/browse/branches/firebug1.6/content/firebug/lib.js#1332
 * as suggested at 
 * http://stackoverflow.com/questions/3454526/how-to-calculate-the-xpath-position-of-an-element-using-javascript/3454564#3454564
 * 
 */
xpathUtil = function() {
////////////////////////////// 
  
// ************************************************************************************************
// XPath
  
/**
 * Gets an XPath for an element which describes its hierarchical location.
 */
this.getElementXPath = function(element)
{
    if (element && element.id)
        return '//*[@id="' + element.id + '"]';
    else
        return this.getElementTreeXPath(element);
};

this.getElementTreeXPath = function(element)
{
    var paths = [];

    // Use nodeName (instead of localName) so namespace prefix is included (if any).
    for (; element && element.nodeType == 1; element = element.parentNode)
    {
        // Once we hit an ID, stop going up
        if (element.id) {
          paths.splice(0, 0, '//*[@id="' + element.id + '"]');
          return paths.join("/");
        }
      
        var index = 0;
        for (var sibling = element.previousSibling; sibling; sibling = sibling.previousSibling)
        {
            // Ignore document type declaration.
            if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE)
                continue;

            if (sibling.nodeName == element.nodeName)
                ++index;
        }

        var tagName = element.nodeName.toLowerCase();
        var pathIndex = (index ? "[" + (index+1) + "]" : "");
        paths.splice(0, 0, tagName + pathIndex);
    }

    return paths.length ? "/" + paths.join("/") : null;
};

////////////////////////////// end verbatim copy
};
var xpathGetter = new xpathUtil();
