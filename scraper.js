(function ($) {
///////////////////////////////////////////////////////////////////////////////

/**
 * Analysis tools for scraping a page content and theme
 * 
 * Provides a form for presenting the scraped sections and data
 */

// ////////////////////////////////
// Worker object & methods

/**
 * An object that displays the data (scraper rules) captured so far.
 * 
 */
ScraperControl = function(container, options) {
  this.rules = new Array();
  this.elementID = 'scraper-control';
  this.activeRulesetName = 'Default';
  // Handles on parts of the UI
  this.uiSections = new Array();
  
  if (container) {
    this.container = container;
  }
  else {
    this.container = $('body');
  }

};



/**
 * Styles and positions the dashboard
 */
ScraperControl.prototype.build = function() {
  $.log('Building Scraper UI', 'group');
  this.element = $('<div>').attr('id', this.elementID);
  this.container.append(this.element);
  this.buildRulesUI(this.rules);
  $.log('Scraper UI built', 'grend');
};


/**
 * Trivial accessors
 */
ScraperControl.prototype.loadRules = function(rules) {
  this.rules = rules;
};
ScraperControl.prototype.getActiveRules = function() {
  return this.rules[this.activeRulesetName];
};

/**
 * Less trivial accessors. Getters and setters for the ruleset.
 */
/**
 * Add a new pattern rule for extracting data from the named source. Update the
 * configs, and update the UI to show it
 */
ScraperControl.prototype.addSourceRule = function(sourceID, xpath, label) {
  label = label ? label : xpath;
  $.log('Adding new xpath extraction rule (' + xpath + ') called "' + label
      + '" to the ' + sourceID + " element for parsing");
  this.rules[this.activeRulesetName]['parser']['config']['sources'][sourceID][label] = xpath;

  // Update/rebuild the UI.
  var elementRules = this.rules[this.activeRulesetName]['parser']['config']['sources'][sourceID];
  var sourceDetail = this.buildSourceDetail(elementRules, sourceID);
  if (!this.uiSections[sourceID]) {
    this.addSection(sourceID, sourceDetail, this.elementsDetails, 'h4');
    // re-tabify
    this.elementsDetails.accordion('destroy').accordion({
      'header' : 'h4',
      'autoHeight' : false,
      'collapsible' : true
    });
  } else {
    this.updateSection(sourceID, sourceDetail);
  }
  
  $.log('activate ' + this.safeID(sourceID));
  // Ensure this new thing gets shown in the active UI. Trigger accordion opening.
  this.UIContainer.accordion("activate", '#scraper-Elements-heading');
  this.elementsDetails.accordion("activate", '#' + this.safeID(sourceID)
      + '-heading')
      // highlight the new rule
  var patternID = 'scraper-pattern-' + this.safeID(sourceID + '-' + label);
  $('#' + patternID).fadeOut(0).fadeIn(1000);
  

};

/**
 * Initialize the div that holds the form that displays the rules
 */
ScraperControl.prototype.buildRulesUI = function(rules) {
  $.log('Starting scraper buildRulesUI', 'group');
  this.UIContainer = $('<div>').attr('id', this.elementID);
  this.element.append(this.UIContainer);

  this.sourceTypeSelector = this.buildSourceTypeSelector();
  this.addSection('Source type', this.sourceTypeSelector, this.UIContainer);

  this.pathSelector = this.buildPathSelector();
  this.addSection('Paths', this.pathSelector, this.UIContainer);

  this.elementsDetails = this.buildElementsDetails();
  this.addSection('Elements', this.elementsDetails, this.UIContainer);
  // Need to add to the document before triggereing the accordion init
  // I want collapsible, however that seems to make 'activate' behave as a 'toggle'  
  this.elementsDetails.accordion({
    'header' : 'h4',
    'autoHeight' : false,
    //'collapsible' : true
  });

  this.UIContainer.accordion({
    'autoHeight' : false,
    //'collapsible' : true,
    clearStyle : true,
    'active' : 2
  });
  $.log('Finished scraper buildRulesUI', 'grend');

};


/**
 * Return a div containing a source type selector (Mock up)
 */
ScraperControl.prototype.buildSourceTypeSelector = function() {
  this.pathSelector = $('<div id="scraper-sourceTypeSelector"></div>');
  var list = $('<dl>');
  this.pathSelector.append(list);
  sourceTypes = {'Static':'Static', 'Joomla':'Joomla', 'Drupal4':'Drupal4', '<b>Silverstripe</b>':'Silverstripe', 'Wordpress':'Wordpress', 'SharePoint':'SharePoint', 'MCMS':'MCMS', 'MySourceMatrix':'MySourceMatrix', 'Plone':'Plone'};
  for ( var ruleName in sourceTypes) {
    list.append($('<dt>').html(ruleName));
    //rule = this.rules[ruleName];
    //list.append($('<dd>').html(ruleName));
  }
  $.log('Built the source type profile selector', 'debug');
  return this.pathSelector;
};


/**
 * Return a div containing a path selector and manager, using current known
 * rules.
 */
ScraperControl.prototype.buildPathSelector = function() {
  this.pathSelector = $('<div id="scraper-pathSelector"></div>');
  //this.pathSelector.append('<span>path selector here</span>');
  var list = $('<dl>');
  this.pathSelector.append(list);
  for ( var ruleName in this.rules) {
    list.append($('<dt>').text(ruleName));
    rule = this.rules[ruleName];
    list.append($('<dd>').text(rule['pathPattern']));
  }
  $.log('Built the path profile selector', 'debug');
  return this.pathSelector;
};

/**
 * Return a div containing details about each of the elements that can be
 * extracted
 */
ScraperControl.prototype.buildElementsDetails = function() {
  this.elementsDetails = $('<div id="scraper-elementsDetails"></div>');
  var activeRules = this.getActiveRules();

  for ( var sourceID in activeRules['parser']['config']['sources']) {
    var elementRules = activeRules['parser']['config']['sources'][sourceID];
    var sourceDetail = this.buildSourceDetail(elementRules, sourceID);
    this.addSection(sourceID, sourceDetail, this.elementsDetails, 'h4');
  }

  $.log('Built the divs for each element that can be scanned for', 'debug');
  return this.elementsDetails;
};

/**
 * Singular subroutine for buildelementsDetails.
 * 
 * Render the precise scraping and mapping rule(s) for one element
 * 
 * @param sourceDetail
 *          a small array describing the scrape patterns of this element data
 *          source
 */
ScraperControl.prototype.buildSourceDetail = function(sourceDetail, sourceID) {
  var elementDescription = $('<div>').addClass('scraper-elementDetail');
  var scrapeRules = $('<h5>Scrape pattern(s) : </h5>');
  scrapeRules.attr('title', 'Click to run the scrape');
  scrapeRules.click(function() {
    scraper.scrapeSource(sourceID);
  });
  elementDescription.append(scrapeRules);
  $.log(sourceDetail);

  // For each of the patterns, we need a test, edit, and delete option
  for ( var patternID in sourceDetail) {
    var patternRow = $('<div>')
      .addClass('scraper-pattern')
      .attr('id', 'scraper-pattern-' + this.safeID(sourceID + '-' + patternID))
      .text(sourceDetail[patternID])
      .attr('title', patternID);
    var pattern = sourceDetail[patternID];
    elementDescription.append(patternRow);
    // Action button
    var scrapeButton = $('<span>')
      .button({
        icons : {
          'primary' : 'ui-icon-search'
        },
        text : false
      })
      .attr('title', 'Scrape content according to this ' + patternID + ' rule')
      .data({
        'sourceID' : sourceID,
        'patternID' : patternID,
        'pattern' : pattern
      })
      .click(function() {
        // Needed to insert the relevant data into the element so it could be
        // retrieved later
        // the variable gets passed around via enclosure context otherwise,
        // which is not the result I want
        var sourceID = $(this).data('sourceID');
        var pattern = $(this).data('pattern');
        scraper.scrapeSource(sourceID, pattern);
      });
    patternRow.append(scrapeButton);
  }
  return elementDescription;
};

/**
 * Run the extraction pattern for the given mapped element on the source page.
 * 
 * Highlights the found element(s) and sucks the data to the scraper for
 * inspection.
 */
ScraperControl.prototype.scrapeElement = function(elementDetail) {
  $.log('Scraping document looking for content for the '
      + elementDetail['mapping']);
  // unpack xpaths array into keys only
  $.log(elementDetail);
  var xpathPatterns = new Array();
  for ( var key in elementDetail['xpaths']) {
    xpathPatterns.push(key);
  }
  // scrunch the array for jquery
  var xpathPattern = xpathPatterns.join(', ');

  $.log(xpathPattern);
  var foundElements = director.findElement(xpathPattern);
  director.highlightElement(foundElements);
  this.showHTMLPreview(foundElements);
};

/*
 * Grab the content of the selected element(s), show in a pop-up. 
 * Re-use just one dialog?
 */
ScraperControl.prototype.showHTMLPreview = function(foundElements) {

  // Remove our own markers from it, that were added by showIDs
  // foundElements.remove('.id-label'); // different jquery version? doesn't support the arg to remove()
  foundElements.find('.id-label').remove();
  var found_html;
  if (!foundElements.length) {
    var scraped_summary = $('<div>').attr('title', 'No data').html('No data found');
    scraped_summary.css('max-height', '400px').dialog({
      'dialogClass' : 'scraped-content-preview',
      'width' : '80%',
    })
  }
  else {
    found_html = "" + foundElements.html();
    var scraped_summary = $('<div>').attr('title', 'Scraped content').text(found_html);
      scraped_summary.css('max-height', '400px').dialog({
      'dialogClass' : 'scraped-content-preview',
      'width' : '80%',
      // resize event is a hack around for unsupported max-height issue
      // http://stackoverflow.com/questions/4032468/adjust-height-on-jqueryui-dialog-to-be-as-tall-as-content-up-to-a-max/8152085#8152085
      'resize' : function(event, ui) {
        $(this).css('max-height', '1000px');
        $.log('resize');
      }
    });
    $.log(scraped_summary.html());
  }
}

/**
 * Run the extraction pattern for the given mapped element on the source page.
 * 
 * Highlights the found element(s) and sucks the data to the scraper for
 * inspection.
 */
ScraperControl.prototype.scrapeSource = function(sourceID, pattern) {
  $.log('Scraping document. looking for content for the "' + sourceID + '"');
  $.log($(this).data('pattern'));
  if (pattern) {
    $.log('Using specified pattern "' + pattern + '"');
  } else {
    // Fetch extraction pattern settings from the current configs
    var ruleset = this.getActiveRules();
    var patterns = ruleset['parser']['config']['sources'][sourceID];
    // Scrunch the array for jquery. Boring I can't join() an object/array
    var patternsArray = Array();
    for (label in patterns) {
      patternsArray.push(patterns[label]);
    }
    pattern = patternsArray.join(', ');

    $.log('Using full settings pattern ' + pattern);
  }

  var foundElements = director.findElement(pattern);
  director.highlightElement(foundElements);
  this.showHTMLPreview(foundElements);
};

/**
 * Adds the given content as a section in the accordion layout Utility helper
 * for accordion.
 */
ScraperControl.prototype.addSection = function(heading, content, container,
    headertag) {
  if (!container) {
    container = this.element;
  }
  if (!headertag) {
    headertag = 'h3';
  }
  var id = this.safeID(heading);
  container.append($('<' + headertag + '>').attr('id', id + '-heading').append(
      $('<a>').attr('href', id).text(heading)));
  var content_div = $('<div>').attr('id', id);
  content_div.append(content);
  container.append(content_div);
  // Keep a handle on this section so we can find it again
  this.uiSections[heading] = content_div;
};

/**
 * Minor Util to turn headings into css-safe IDs
 */
ScraperControl.prototype.safeID = function(text) {
  return 'scraper-' + text.replace(/[^a-zA-Z0-9]+/g, '-');
};

/**
 * Update the content of an already existing built element in the sections UI.
 */
ScraperControl.prototype.updateSection = function(heading, content) {
  $(this.uiSections[heading]).html(content);
};

/**
 * Add visible labels to any elements with an ID
 */
ScraperControl.prototype.showIDs = function() {
  // Only apply once, toggle if clicked twice
  if (director.findElement("body").hasClass('showing-ids')) {
    this.hideIDs();
    return;
  }
  director.findElement("body").addClass('showing-ids');
  
  $.log('Enumerating elements with any IDs');
  var elements = director.findElement("*[id]")
    .not('.ui-menu-item')
    .not('#jqContextMenu');
  // Exclude UI buttons (contextmenu) from that
  // $.log(elements)

  // Create a right-click menu. Once only.
  var menuID = 'menu-add-mapping';
  if (! this.menuBuilt) {
    // Inject the jquery-ui css for formatting menus to the page?
    //
    // Build a context menu list of buttons, one for each mapping target.
    // Attach that context menu to each possible source element
    var rules = this.getActiveRules();
  
    // Setup for using jquery.contextmenu-ui.js
    var menu = $('<div><ul></ul></div>')
      .attr('id', menuID)
      .addClass('contextMenu')
      .hide();
    //$('body').append(menu);
    director.findElement("body").append(menu);
  
    
    //   Fucking closures are killing me.
    //   Need to fake a function that returns a function so that the made function 
    //   remembers the ID, not a reference to the ID.
    makeButtonCallback = function(sourceID) {
      return function (trigger, currentTarget) {
        // trigger is the real element. currentTarget is just where we happened to click.
        // Calculate a preliminary xpath for this item
        var xpath = xpathGetter.getElementXPath(trigger);
        $.log('Adding a mapping rule (' + xpath + ') -> "'
            + sourceID + '" to the scraper');
        scraper.addSourceRule(sourceID, xpath, 'XPath: ' + xpath);
      };
    }
    
    // List the rules that will be attached to the context menu for each item
    // Create an action callback, and a corresponding li for each one.
    var buttonCallbacks = {};
    for (sourceID in rules['parser']['config']['sources']) {
      var safeID = sourceID.replace(/[^\w]/g, "_");
      label = 'Assign to ' + sourceID;
  
      // Invoke the function that needs to be a closure
      buttonCallbacks[safeID] = makeButtonCallback(sourceID);
      
      var button = $('<li></li>')
        .attr('id', safeID)
        .addClass('ui-menu-item')
        .append(label);
      $('ul', menu).append(button);
    }
    this.menuBuilt = true;
  }

  // Bind this menu behavior to all significant elements inh the target page 
  // (and highlight them)
  elements.each(function() {
    $(this)
      .addClass('id-element')
      .append(
        $('<div>').addClass('id-label').text($(this).attr('id')))
        .contextMenu(menuID, {
          bindings: buttonCallbacks,
          menuStyle: {
            listStyle: 'none',
            padding: '1px',
            margin: '0px',
            backgroundColor: '#fff',
            border: '1px solid #999',
            width: '200px'
          },
          itemStyle: {
            margin: '0px',
            color: '#000',
            display: 'block',
            cursor: 'default',
            padding: '3px',
            border: '1px solid #fff',
            backgroundColor: 'transparent'
          },
          itemHoverStyle: {
            border: '1px solid #0a246a',
            backgroundColor: '#b6bdd2'
          },
          menuShadowStyle: {
            backgroundColor: 'rgb(0, 0, 0)',
            opacity: '0.2',
            padding: "1px 1px"
          }
          
        })
  });
  // The (unstable?) version of contextmenu I found wants to use jqueryUI 
  // styling, but fails. Instead, add the css here, derived from the earty
  // http://www.trendskitchens.co.nz/jquery/contextmenu/ version.
  
    
} // method

ScraperControl.prototype.hideIDs = function() {
  // Only apply once, toggle if clicked twice
  if (! director.findElement("body").hasClass('showing-ids')) {
    this.showIDs();
    return;
  }
  director.findElement('.id-element')
    .removeClass('id-element')
  director.findElement('.id-label')
    .remove();
  director.findElement("body").removeClass('showing-ids');
  $.log('removed all div highlighting');
}

///////////////////////////////////////////////////////////////////////////////
})(jQuery);
